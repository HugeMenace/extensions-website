from django import forms


class SubscribeNotificationEmailsForm(forms.Form):
    subscribe = forms.BooleanField(widget=forms.HiddenInput(), required=False)
