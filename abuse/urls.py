from django.urls import path, re_path, include

from . import views
from constants.base import EXTENSION_SLUGS_PATH

app_name = 'abuse'
urlpatterns = [
    path('abuse/reports/', views.ReportList.as_view(), name='report-list'),
    path('abuse/reports/<int:pk>/', views.ReportView.as_view(), name='view-report'),
    re_path(
        rf'^(?P<type_slug>{EXTENSION_SLUGS_PATH})/',
        include(
            [
                path(
                    '<slug:slug>/report/',
                    views.ReportExtensionView.as_view(),
                    name='report-extension',
                ),
                path(
                    '<slug:slug>/<str:version>/<int:rating>/report/',
                    views.ReportRatingView.as_view(),
                    name='report-ratings',
                ),
            ],
        ),
    ),
    # TODO: report user view and URL
]
