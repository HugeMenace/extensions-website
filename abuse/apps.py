from django.apps import AppConfig


class AbuseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'abuse'

    def ready(self):
        from actstream import registry
        import abuse.signals  # noqa: F401

        registry.register(self.get_model('AbuseReport'))
