from actstream import action
from actstream.actions import follow
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from constants.activity import Flag, Verb
from reviewers.models import ApprovalActivity


@receiver(post_save, sender=ApprovalActivity)
def _create_action_from_review_and_follow(
    sender: object,
    instance: ApprovalActivity,
    created: bool,
    raw: bool,
    **kwargs: object,
) -> None:
    if raw:
        return
    if not created:
        return

    # automatically follow after an interaction
    # if a user had unfollowed this extension before,
    # we are making them a follower again
    follow(instance.user, instance.extension, send_action=False, flag=Flag.REVIEWER)

    activity_type2verb = {
        ApprovalActivity.ActivityType.APPROVED: Verb.APPROVED,
        ApprovalActivity.ActivityType.AWAITING_CHANGES: Verb.REQUESTED_CHANGES,
        ApprovalActivity.ActivityType.AWAITING_REVIEW: Verb.REQUESTED_REVIEW,
        ApprovalActivity.ActivityType.COMMENT: Verb.COMMENTED,
        ApprovalActivity.ActivityType.UPLOADED_NEW_VERSION: Verb.UPLOADED_NEW_VERSION,
    }
    action.send(
        instance.user,
        verb=activity_type2verb.get(instance.type),
        action_object=instance,
        target=instance.extension,
    )


@receiver(pre_delete, sender=ApprovalActivity)
def _log_deletion(sender: object, instance: ApprovalActivity, **kwargs: object) -> None:
    instance.record_deletion()
