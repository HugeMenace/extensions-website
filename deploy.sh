#!/bin/bash

ENVIRONMENT=$1

# Shift the arguments to access the remaining optional arguments with $@
shift

if [ -z "$ENVIRONMENT" ] || [ "$ENVIRONMENT" != "production" -a "$ENVIRONMENT" != "staging" ]
then
      echo "Usage: ./deploy.sh staging|production"
      exit 1
fi

if [ "$ENVIRONMENT" = "production" ]
then
    git fetch origin main:production && git push origin production
fi

pushd playbooks
source .venv/bin/activate
# Turn on verbose mode
set -x
./ansible.sh -i environments/$ENVIRONMENT deploy.yaml $@
deactivate
popd
