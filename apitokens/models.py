import hashlib
import secrets

from django.db import models
from django.urls import reverse

from users.models import User


class UserToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tokens')
    name = models.CharField(max_length=255)
    token_prefix = models.CharField(max_length=5, editable=False)
    token_hash = models.CharField(max_length=64, editable=False, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_last_access = models.DateTimeField(null=True, blank=True, editable=False)
    ip_address_last_access = models.GenericIPAddressField(protocol='both', null=True)

    def get_delete_url(self):
        return reverse('apitokens:delete', kwargs={'pk': self.pk})

    def __str__(self):
        return f"{self.user.username} - {self.token_prefix} - {self.name}"

    @staticmethod
    def generate_hash(token_key: str) -> str:
        return hashlib.sha256(token_key.encode()).hexdigest()

    @staticmethod
    def generate_token_key() -> str:
        return secrets.token_urlsafe(32)

    @classmethod
    def generate_token_prefix(cls, token_key: str) -> str:
        token_prefix_length = cls._meta.get_field('token_prefix').max_length
        return token_key[:token_prefix_length]
