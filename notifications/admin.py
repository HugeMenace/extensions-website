from django.contrib import admin

from notifications.models import Notification


class NotificationAdmin(admin.ModelAdmin):
    readonly_fields = (
        'recipient',
        'action',
        'email_sent',
        'processed_by_mailer_at',
        'read_at',
    )
    fields = readonly_fields

    def get_queryset(self, request):
        return Notification.objects.all()


admin.site.register(Notification, NotificationAdmin)
