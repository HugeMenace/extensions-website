"""Create all necessary follow records."""
import logging

from actstream.actions import follow
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from constants.activity import Flag
from extensions.models import Extension
from reviewers.models import ApprovalActivity

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Command(BaseCommand):
    def handle(self, *args, **options):  # noqa: D102
        # TODO? keep a record of explicit unfollow requests to avoid re-following
        extensions = Extension.objects.all()
        moderators = Group.objects.get(name='moderators').user_set.all()
        for extension in extensions:
            authors = extension.authors.all()
            for recipient in authors:
                _follow_with_log(recipient, extension, Flag.AUTHOR)
            for recipient in moderators:
                _follow_with_log(recipient, extension, Flag.MODERATOR)

        approval_activity_items = ApprovalActivity.objects.all().select_related('extension', 'user')
        for item in approval_activity_items:
            _follow_with_log(item.user, item.extension, Flag.REVIEWER)


def _follow_with_log(user, target, flag):
    follow(user, target, send_action=False, flag=flag)
    logger.info(f'{user} follows {target} with flag={flag}')
