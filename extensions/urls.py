from django.urls import path, re_path, include, reverse_lazy
from django.views.generic import RedirectView

from constants.base import EXTENSION_SLUGS_PATH
from extensions.views import api, public, submit, manage

app_name = 'extensions'
urlpatterns = [
    path('submit/', submit.UploadFileView.as_view(), name='submit'),
    # TODO: move /accounts pages under its own app when User model is replaced
    path('accounts/extensions/', manage.ManageListView.as_view(), name='manage-list'),
    # FIXME: while there's no profile page, redirect to My Extensions instead
    path(
        'accounts/profile/',
        RedirectView.as_view(url=reverse_lazy('extensions:manage-list'), permanent=False),
    ),
    # API
    path('api/v1/extensions/', api.ExtensionsAPIView.as_view(), name='api'),
    path(
        'api/v1/extensions/<str:extension_id>/versions/new/',
        api.UploadExtensionVersionView.as_view(),
        name='upload-extension-version',
    ),
    # Public pages
    path('', public.HomeView.as_view(), name='home'),
    path('search/', public.SearchView.as_view(), name='search'),
    path('author/<int:user_id>/', public.SearchView.as_view(), name='by-author'),
    path('search/', public.SearchView.as_view(), name='search'),
    path('tag/<slug:tag_slug>/', public.SearchView.as_view(), name='by-tag'),
    path('team/<slug:team_slug>/', public.SearchView.as_view(), name='by-team'),
    re_path(
        rf'^(?P<type_slug>{EXTENSION_SLUGS_PATH})/',
        include(
            [
                path('<slug:slug>/', public.ExtensionDetailView.as_view(), name='detail'),
            ]
        ),
    ),
    re_path(
        rf'^(?P<type_slug>{EXTENSION_SLUGS_PATH})/$',
        public.SearchView.as_view(),
        name='by-type',
    ),
    # Pages for maintainers
    re_path(
        rf'^(?P<type_slug>{EXTENSION_SLUGS_PATH})/',
        include(
            [
                path(
                    '<slug:slug>/draft/',
                    manage.DraftExtensionView.as_view(),
                    name='draft',
                ),
                path(
                    '<slug:slug>/manage/',
                    manage.UpdateExtensionView.as_view(),
                    name='manage',
                ),
                path(
                    '<slug:slug>/delete/',
                    manage.DeleteExtensionView.as_view(),
                    name='delete',
                ),
                path(
                    '<slug:slug>/manage/versions/',
                    manage.ManageVersionsView.as_view(),
                    name='manage-versions',
                ),
                path(
                    '<slug:slug>/manage/versions/new/',
                    manage.NewVersionView.as_view(),
                    name='new-version',
                ),
                path(
                    '<slug:slug>/manage/versions/new/<int:pk>/',
                    manage.NewVersionFinalizeView.as_view(),
                    name='new-version-finalise',
                ),
                path(
                    '<slug:slug>/manage/versions/<int:pk>/delete/',
                    manage.VersionDeleteView.as_view(),
                    name='version-delete',
                ),
                path(
                    '<slug:slug>/manage/versions/<int:pk>/update/',
                    manage.UpdateVersionView.as_view(),
                    name='version-update',
                ),
                path(
                    '<slug:slug>/<version>/download/<filename>',
                    public.extension_version_download,
                    name='version-download',
                ),
                path('<slug:slug>/versions/', manage.VersionsView.as_view(), name='versions'),
            ],
        ),
    ),
]
