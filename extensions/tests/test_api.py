from pathlib import Path

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from common.tests.factories.users import UserFactory
from common.tests.factories.extensions import create_approved_version
from common.tests.utils import create_user_token

from extensions.models import Version


TEST_FILES_DIR = Path(__file__).resolve().parent / 'files'


class VersionUploadAPITest(APITestCase):
    def setUp(self):
        self.user = UserFactory()
        self.token, self.token_key = create_user_token(user=self.user)

        self.client = APIClient()
        self.version = create_approved_version(
            extension__extension_id="amaranth",
            version="1.0.7",
            file__user=self.user,
        )
        self.extension = self.version.extension
        self.file_path = TEST_FILES_DIR / "amaranth-1.0.8.zip"

    @staticmethod
    def _get_upload_url(extension_id):
        upload_url = reverse('extensions:upload-extension-version', args=(extension_id,))
        return upload_url

    def test_version_upload_unauthenticated(self):
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(self.extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
            )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_version_upload_extension_not_maintained_by_user(self):
        other_user = UserFactory()
        other_extension = create_approved_version(
            extension__extension_id='other_extension', file__user=other_user
        ).extension

        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(other_extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.data['message'],
            f'Extension "{other_extension.extension_id}" not maintained by user "{self.user.username}"',
        )

    def test_version_upload_extension_does_not_exist(self):
        extension_name = 'extension_do_not_exist'
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(extension_name),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data['message'], f'Extension "{extension_name}" not found')

    def test_version_upload_success(self):
        self.assertEqual(Version.objects.filter(extension=self.extension).count(), 1)
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(self.extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Version.objects.filter(extension=self.extension).count(), 2)

    def test_date_last_access(self):
        self.assertIsNone(self.token.date_last_access)
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(self.extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.token.refresh_from_db()
        self.assertIsNotNone(self.token.date_last_access)
