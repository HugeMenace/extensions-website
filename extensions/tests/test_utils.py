from django.test import TestCase

from extensions.utils import clean_json_dictionary_from_optional_fields


class UtilsTest(TestCase):
    def test_clean_json_dictionary(self):
        test_dictionary = {
            'tagline': '',
            'description': 'my description',
            'permissions': [],
            'tags': ['foo', 'bar'],
            'license': ['SPDX:GPL-2.0-or-later'],
            'blender_version_min': "2.9.3",
            'blender_version_max': None,
        }
        expected_clean_dictionary = {
            'tagline': '',
            'description': 'my description',
            'tags': ['foo', 'bar'],
            'license': ['SPDX:GPL-2.0-or-later'],
            'blender_version_min': "2.9.3",
        }
        cleaned_dictionary = clean_json_dictionary_from_optional_fields(test_dictionary)
        self.assertDictEqual(expected_clean_dictionary, cleaned_dictionary)

    def test_clean_json_dictionary_empty_tags(self):
        test_dictionary = {
            'permissions': [],
            'license': [],
        }
        expected_clean_dictionary = {
            'license': [],
        }
        cleaned_dictionary = clean_json_dictionary_from_optional_fields(test_dictionary)
        self.assertDictEqual(expected_clean_dictionary, cleaned_dictionary)
