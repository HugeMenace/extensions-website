from django.contrib import admin

import teams.models


class TeamsUsersInline(admin.TabularInline):
    model = teams.models.TeamsUsers
    raw_id_fields = ('user',)
    extra = 0


@admin.register(teams.models.Team)
class TeamAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('name', 'slug', 'user_count', 'date_created')
    list_display_links = ['name']
    list_filter = ['date_created']
    search_fields = ('id', '^slug', 'name')
    inlines = [TeamsUsersInline]
    prepopulated_fields = {'slug': ('name',)}
    view_on_site = True

    def user_count(self, obj):
        """Display number of users in the team."""
        return obj.users.all().count()

    user_count.short_description = "Users"
