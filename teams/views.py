"""Team pages."""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import ListView
from django.views.generic.detail import DetailView

from extensions.models import Extension
from teams.models import Team, TeamsUsers


class TeamsView(LoginRequiredMixin, ListView):
    model = Team

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['team_memberships'] = (
            self.request.user.team_users.select_related('team').order_by('team__name').all()
        )
        return context


class LeaveTeamView(LoginRequiredMixin, DetailView):
    model = Team
    template_name = 'teams/confirm_leave.html'

    def post(self, request, *args, **kwargs):
        team = self.get_object()
        team_user = TeamsUsers.objects.filter(team=team, user=self.request.user).first()
        if team_user and team_user.may_leave:
            team_user.delete()
        return redirect('teams:list')

    def get_context_data(self, **kwargs):
        team = self.get_object()
        team_user = TeamsUsers.objects.filter(team=team, user=self.request.user).first()
        context = super().get_context_data(**kwargs)
        context['may_leave'] = team_user.may_leave
        context['will_lose_access_to'] = list(
            Extension.objects.authored_by(self.request.user).exclude(
                maintainer__user_id=self.request.user.pk
            )
        )
        return context
