from django.urls import path

import teams.views

app_name = 'teams'
urlpatterns = [
    path('settings/teams/', teams.views.TeamsView.as_view(), name='list'),
    path(
        'settings/leave-team/<slug:slug>/',
        teams.views.LeaveTeamView.as_view(),
        name='leave-team',
    ),
]
